module Data.SpirV.Reflect.Traits where

import Data.SpirV.Enum qualified as SpirV
import Data.SpirV.Enum.Dim qualified as Dim
import Data.SpirV.Enum.ImageFormat qualified as ImageFormat
import Data.Vector.Storable qualified as Storable
import Data.Word (Word32)
import GHC.Generics (Generic)

data Numeric = Numeric
  { scalar :: Scalar
  , vector :: Vector
  , matrix :: Matrix
  }
  deriving (Eq, Ord, Show, Generic)

emptyNumeric :: Numeric
emptyNumeric =
  Numeric
  { scalar = emptyScalar
  , vector = emptyVector
  , matrix = emptyMatrix
  }

data Scalar = Scalar
  { width  :: Word32
  , signed :: Bool
  }
  deriving (Eq, Ord, Show, Generic)

emptyScalar :: Scalar
emptyScalar = Scalar
  { width = 0
  , signed = False
  }

data Vector = Vector
  { component_count :: Word32
  }
  deriving (Eq, Ord, Show, Generic)

emptyVector :: Vector
emptyVector = Vector
  { component_count = 0
  }

data Matrix = Matrix
  { column_count :: Word32
  , row_count    :: Word32
  , stride       :: Word32 -- ^ Measured in bytes
  }
  deriving (Eq, Ord, Show, Generic)

emptyMatrix :: Matrix
emptyMatrix = Matrix
  { column_count = 0
  , row_count = 0
  , stride = 0
  }

data Array = Array
  { dims_count :: Word32
  , dims       :: Storable.Vector Word32
  , stride     :: Maybe Word32
  }
  deriving (Eq, Ord, Show, Generic)

emptyArray :: Array
emptyArray = Array
  { dims_count = 0
  , dims = mempty
  , stride = Nothing
  }

data Image = Image
  { dim          :: SpirV.Dim
  , depth        :: Word32
  , arrayed      :: Word32
  , ms           :: Word32 -- ^ 0: single-sampled, 1: multisampled
  , sampled      :: Word32
  , image_format :: SpirV.ImageFormat
  }
  deriving (Eq, Ord, Show, Generic)

emptyImage :: Image
emptyImage = Image
  { dim = Dim.Dim1D
  , depth = 0
  , arrayed = 0
  , ms = 0
  , sampled = 0
  , image_format = ImageFormat.Unknown
  }
