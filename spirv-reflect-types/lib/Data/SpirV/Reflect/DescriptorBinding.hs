module Data.SpirV.Reflect.DescriptorBinding
  ( DescriptorBinding(..)
  , WordOffset(..)
  ) where

import Data.Text (Text)
import Data.Vector (Vector)
import Data.Word (Word32)
import GHC.Generics (Generic)

import Data.SpirV.Reflect.BlockVariable (BlockVariable)
import Data.SpirV.Reflect.Enums qualified as Enums
import Data.SpirV.Reflect.Traits qualified as Traits
import Data.SpirV.Reflect.TypeDescription (TypeDescription)

data DescriptorBinding = DescriptorBinding
  { spirv_id                    :: Maybe Word32
  , name                        :: Text
  , binding                     :: Word32
  , input_attachment_index      :: Word32
  , set                         :: Word32
  , descriptor_type             :: Enums.DescriptorType
  , resource_type               :: Enums.ResourceFlags
  , image                       :: Traits.Image
  , block                       :: Maybe BlockVariable
  , array                       :: Traits.Array
  , count                       :: Maybe Word32
  , accessed                    :: Word32
  , uav_counter_id              :: Word32
  , uav_counter_binding         :: Maybe DescriptorBinding
  , byte_address_buffer_offsets :: Vector Word32
  , type_description            :: Maybe TypeDescription
  , word_offset                 :: WordOffset
  , decoration_flags            :: Enums.DecorationFlags
  , user_type                   :: Maybe Enums.UserType
  }
  deriving (Eq, Show, Generic)

data WordOffset = WordOffset
  { binding :: Word32
  , set     :: Word32
  }
  deriving (Eq, Show, Generic)
