module Data.SpirV.Reflect.SpecializationConstant
  ( SpecializationConstant(..)
  ) where

import Data.Text (Text)
import Data.Word (Word32)
import GHC.Generics (Generic)

data SpecializationConstant = SpecializationConstant
  { spirv_id    :: Maybe Word32
  , constant_id :: Word32
  , name        :: Maybe Text
  }
  deriving (Eq, Show, Generic)
