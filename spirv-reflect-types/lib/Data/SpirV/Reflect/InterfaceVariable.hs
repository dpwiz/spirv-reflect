module Data.SpirV.Reflect.InterfaceVariable
  ( InterfaceVariable(..)
  , WordOffset(..)
  ) where

import Data.SpirV.Enum qualified as SpirV
import Data.SpirV.Reflect.Enums qualified as Reflect
import Data.SpirV.Reflect.Traits qualified as Traits
import Data.SpirV.Reflect.TypeDescription (TypeDescription)
import Data.Text (Text)
import Data.Vector (Vector)
import Data.Word (Word32)
import GHC.Generics (Generic)

data InterfaceVariable = InterfaceVariable
  { spirv_id         :: Maybe Word32
  , name             :: Maybe Text
  , location         :: Word32
  , component        :: Maybe Word32
  , storage_class    :: SpirV.StorageClass
  , semantic         :: Maybe Text
  , decoration_flags :: Reflect.DecorationFlags
  , built_in         :: Maybe SpirV.BuiltIn
  , numeric          :: Traits.Numeric
  , array            :: Traits.Array
  , members          :: Vector InterfaceVariable
  , format           :: Reflect.Format
  , type_description :: Maybe TypeDescription
  , word_offset      :: WordOffset
  }
  deriving (Eq, Show, Generic)

newtype WordOffset = WordOffset
  { location :: Word32
  }
  deriving (Eq, Show, Generic)
