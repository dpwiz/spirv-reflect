module Data.SpirV.Reflect.Enums.ShaderStage where

import Control.Monad (guard)

import Data.SpirV.Reflect.Enums.Common

type ShaderStage = ShaderStageFlagBits

newtype ShaderStageFlagBits = ShaderStageFlagBits Flags
  deriving newtype (Eq, Ord, Show, Bits, FiniteBits)

pattern SHADER_STAGE_VERTEX_BIT :: ShaderStageFlagBits
pattern SHADER_STAGE_VERTEX_BIT = ShaderStageFlagBits 0x00000001

pattern SHADER_STAGE_TESSELLATION_CONTROL_BIT :: ShaderStageFlagBits
pattern SHADER_STAGE_TESSELLATION_CONTROL_BIT = ShaderStageFlagBits 0x00000002

pattern SHADER_STAGE_TESSELLATION_EVALUATION_BIT :: ShaderStageFlagBits
pattern SHADER_STAGE_TESSELLATION_EVALUATION_BIT = ShaderStageFlagBits 0x00000004

pattern SHADER_STAGE_GEOMETRY_BIT :: ShaderStageFlagBits
pattern SHADER_STAGE_GEOMETRY_BIT = ShaderStageFlagBits 0x00000008

pattern SHADER_STAGE_FRAGMENT_BIT :: ShaderStageFlagBits
pattern SHADER_STAGE_FRAGMENT_BIT = ShaderStageFlagBits 0x00000010

pattern SHADER_STAGE_COMPUTE_BIT :: ShaderStageFlagBits
pattern SHADER_STAGE_COMPUTE_BIT = ShaderStageFlagBits 0x00000020

pattern SHADER_STAGE_TASK_BIT_NV :: ShaderStageFlagBits
pattern SHADER_STAGE_TASK_BIT_NV = ShaderStageFlagBits 0x00000040

pattern SHADER_STAGE_TASK_BIT_EXT :: ShaderStageFlagBits
pattern SHADER_STAGE_TASK_BIT_EXT = SHADER_STAGE_TASK_BIT_NV -- = VK_SHADER_STAGE_CALLABLE_BIT_EXT

pattern SHADER_STAGE_MESH_BIT_NV :: ShaderStageFlagBits
pattern SHADER_STAGE_MESH_BIT_NV = ShaderStageFlagBits 0x00000080

pattern SHADER_STAGE_MESH_BIT_EXT :: ShaderStageFlagBits
pattern SHADER_STAGE_MESH_BIT_EXT = SHADER_STAGE_MESH_BIT_NV -- = VK_SHADER_STAGE_CALLABLE_BIT_EXT

pattern SHADER_STAGE_RAYGEN_BIT_KHR :: ShaderStageFlagBits
pattern SHADER_STAGE_RAYGEN_BIT_KHR = ShaderStageFlagBits 0x00000100

pattern SHADER_STAGE_ANY_HIT_BIT_KHR :: ShaderStageFlagBits
pattern SHADER_STAGE_ANY_HIT_BIT_KHR = ShaderStageFlagBits 0x00000200

pattern SHADER_STAGE_CLOSEST_HIT_BIT_KHR :: ShaderStageFlagBits
pattern SHADER_STAGE_CLOSEST_HIT_BIT_KHR = ShaderStageFlagBits 0x00000400

pattern SHADER_STAGE_MISS_BIT_KHR :: ShaderStageFlagBits
pattern SHADER_STAGE_MISS_BIT_KHR = ShaderStageFlagBits 0x00000800

pattern SHADER_STAGE_INTERSECTION_BIT_KHR :: ShaderStageFlagBits
pattern SHADER_STAGE_INTERSECTION_BIT_KHR = ShaderStageFlagBits 0x00001000

pattern SHADER_STAGE_CALLABLE_BIT_KHR :: ShaderStageFlagBits
pattern SHADER_STAGE_CALLABLE_BIT_KHR = ShaderStageFlagBits 0x00002000

names :: IsString label => ShaderStageFlagBits -> [label]
names bits = do
  (flag, name) <- bitNames
  guard $ bits .&&. flag
  pure name

bitNames :: IsString label => [(ShaderStageFlagBits, label)]
bitNames =
  [ (SHADER_STAGE_VERTEX_BIT, "VERTEX")
  , (SHADER_STAGE_TESSELLATION_CONTROL_BIT, "TESSELLATION_CONTROL")
  , (SHADER_STAGE_TESSELLATION_EVALUATION_BIT, "TESSELLATION_EVALUATION")
  , (SHADER_STAGE_GEOMETRY_BIT, "GEOMETRY")
  , (SHADER_STAGE_FRAGMENT_BIT, "FRAGMENT")
  , (SHADER_STAGE_COMPUTE_BIT, "COMPUTE")
  , (SHADER_STAGE_TASK_BIT_NV, "TASK")
  , (SHADER_STAGE_MESH_BIT_NV, "MESH")
  , (SHADER_STAGE_RAYGEN_BIT_KHR, "RAYGEN")
  , (SHADER_STAGE_ANY_HIT_BIT_KHR, "ANY_HIT")
  , (SHADER_STAGE_CLOSEST_HIT_BIT_KHR, "CLOSEST_HIT")
  , (SHADER_STAGE_MISS_BIT_KHR, "MISS")
  , (SHADER_STAGE_INTERSECTION_BIT_KHR, "INTERSECTION")
  , (SHADER_STAGE_CALLABLE_BIT_KHR, "CALLABLE")
  ]
