module Data.SpirV.Reflect.Enums.Common
  ( Flags
  , (.&&.)

  , Word32
  , Int32

  , IsString
  , Bits
  , FiniteBits

  , toLabel
  ) where

import Data.Word (Word32)
import Data.Bits (Bits, FiniteBits, (.&.))
import Data.Int (Int32)
import Data.String (IsString)
import Data.IntMap qualified as IntMap

type Flags = Word32

(.&&.) :: Bits a => a -> a -> Bool
x .&&. flag = x .&. flag == flag

{-# INLINE toLabel #-}
toLabel :: (Enum i) => [(i, label)] -> i -> Maybe label
toLabel names key = IntMap.lookup (fromEnum key) indexed
  where
    indexed = IntMap.fromList do
      (i, label) <- names
      pure (fromEnum i, label)
