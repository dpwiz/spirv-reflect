module Data.SpirV.Reflect.Enums.TypeFlags where

import Control.Monad (guard)

import Data.SpirV.Reflect.Enums.Common

type TypeFlags = TypeFlagBits

newtype TypeFlagBits = TypeFlagBits Flags
  deriving newtype (Eq, Ord, Show, Bits, FiniteBits)

pattern TYPE_FLAG_UNDEFINED :: TypeFlagBits
pattern TYPE_FLAG_UNDEFINED = TypeFlagBits 0x00000000

pattern TYPE_FLAG_VOID :: TypeFlagBits
pattern TYPE_FLAG_VOID = TypeFlagBits 0x00000001

pattern TYPE_FLAG_BOOL :: TypeFlagBits
pattern TYPE_FLAG_BOOL = TypeFlagBits 0x00000002

pattern TYPE_FLAG_INT :: TypeFlagBits
pattern TYPE_FLAG_INT = TypeFlagBits 0x00000004

pattern TYPE_FLAG_FLOAT :: TypeFlagBits
pattern TYPE_FLAG_FLOAT = TypeFlagBits 0x00000008

pattern TYPE_FLAG_VECTOR :: TypeFlagBits
pattern TYPE_FLAG_VECTOR = TypeFlagBits 0x00000100

pattern TYPE_FLAG_MATRIX :: TypeFlagBits
pattern TYPE_FLAG_MATRIX = TypeFlagBits 0x00000200

pattern TYPE_FLAG_EXTERNAL_IMAGE :: TypeFlagBits
pattern TYPE_FLAG_EXTERNAL_IMAGE = TypeFlagBits 0x00010000

pattern TYPE_FLAG_EXTERNAL_SAMPLER :: TypeFlagBits
pattern TYPE_FLAG_EXTERNAL_SAMPLER = TypeFlagBits 0x00020000

pattern TYPE_FLAG_EXTERNAL_SAMPLED_IMAGE :: TypeFlagBits
pattern TYPE_FLAG_EXTERNAL_SAMPLED_IMAGE = TypeFlagBits 0x00040000

pattern TYPE_FLAG_EXTERNAL_BLOCK :: TypeFlagBits
pattern TYPE_FLAG_EXTERNAL_BLOCK = TypeFlagBits 0x00080000

pattern TYPE_FLAG_EXTERNAL_ACCELERATION_STRUCTURE :: TypeFlagBits
pattern TYPE_FLAG_EXTERNAL_ACCELERATION_STRUCTURE = TypeFlagBits 0x00100000

pattern TYPE_FLAG_EXTERNAL_MASK :: TypeFlagBits
pattern TYPE_FLAG_EXTERNAL_MASK = TypeFlagBits 0x00FF0000

pattern TYPE_FLAG_STRUCT :: TypeFlagBits
pattern TYPE_FLAG_STRUCT = TypeFlagBits 0x10000000

pattern TYPE_FLAG_ARRAY :: TypeFlagBits
pattern TYPE_FLAG_ARRAY = TypeFlagBits 0x20000000

pattern TYPE_FLAG_REF :: TypeFlagBits
pattern TYPE_FLAG_REF = TypeFlagBits 0x40000000

typeFlagBitNames :: IsString label => [(TypeFlagBits, label)]
typeFlagBitNames =
  [ (TYPE_FLAG_UNDEFINED, "UNDEFINED")
  , (TYPE_FLAG_VOID, "VOID")
  , (TYPE_FLAG_BOOL, "BOOL")
  , (TYPE_FLAG_INT, "INT")
  , (TYPE_FLAG_FLOAT, "FLOAT")
  , (TYPE_FLAG_VECTOR, "VECTOR")
  , (TYPE_FLAG_MATRIX, "MATRIX")
  , (TYPE_FLAG_EXTERNAL_IMAGE, "EXTERNAL_IMAGE")
  , (TYPE_FLAG_EXTERNAL_SAMPLER, "EXTERNAL_SAMPLER")
  , (TYPE_FLAG_EXTERNAL_SAMPLED_IMAGE, "EXTERNAL_SAMPLED_IMAGE")
  , (TYPE_FLAG_EXTERNAL_BLOCK, "EXTERNAL_BLOCK")
  , (TYPE_FLAG_EXTERNAL_ACCELERATION_STRUCTURE, "EXTERNAL_ACCELERATION_STRUCTURE")
  , (TYPE_FLAG_EXTERNAL_MASK, "EXTERNAL_MASK")
  , (TYPE_FLAG_STRUCT, "STRUCT")
  , (TYPE_FLAG_ARRAY, "ARRAY")
  , (TYPE_FLAG_ARRAY, "REF")
  ]

typeFlagsNames :: IsString label => TypeFlags -> [label]
typeFlagsNames bits = do
  (flag, name) <- drop 1 typeFlagBitNames
  guard $ bits .&&. flag
  pure name
