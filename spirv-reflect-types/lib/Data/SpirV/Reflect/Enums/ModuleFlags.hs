module Data.SpirV.Reflect.Enums.ModuleFlags where

import Data.SpirV.Reflect.Enums.Common

type ModuleFlags = ModuleFlagBits

newtype ModuleFlagBits = ModuleFlagBits Word32
  deriving newtype (Eq, Ord, Show, Bits, FiniteBits)

pattern MODULE_FLAG_NONE :: ModuleFlagBits
pattern MODULE_FLAG_NONE = ModuleFlagBits 0x00000000

pattern MODULE_FLAG_NO_COPY :: ModuleFlagBits
pattern MODULE_FLAG_NO_COPY = ModuleFlagBits 0x00000001
