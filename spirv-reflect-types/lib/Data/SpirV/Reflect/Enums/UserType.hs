module Data.SpirV.Reflect.Enums.UserType where

import Data.SpirV.Reflect.Enums.Common

newtype UserType = UserType Int
  deriving newtype (Eq, Ord, Show, Enum)

pattern USER_TYPE_INVALID :: UserType
pattern USER_TYPE_INVALID = UserType 0

pattern USER_TYPE_CBUFFER :: UserType
pattern USER_TYPE_CBUFFER = UserType 1

pattern USER_TYPE_TBUFFER :: UserType
pattern USER_TYPE_TBUFFER = UserType 2

pattern USER_TYPE_APPEND_STRUCTURED_BUFFER :: UserType
pattern USER_TYPE_APPEND_STRUCTURED_BUFFER = UserType 3

pattern USER_TYPE_BUFFER :: UserType
pattern USER_TYPE_BUFFER = UserType 4

pattern USER_TYPE_BYTE_ADDRESS_BUFFER :: UserType
pattern USER_TYPE_BYTE_ADDRESS_BUFFER = UserType 5

pattern USER_TYPE_CONSTANT_BUFFER :: UserType
pattern USER_TYPE_CONSTANT_BUFFER = UserType 6

pattern USER_TYPE_CONSUME_STRUCTURED_BUFFER :: UserType
pattern USER_TYPE_CONSUME_STRUCTURED_BUFFER = UserType 7

pattern USER_TYPE_INPUT_PATCH :: UserType
pattern USER_TYPE_INPUT_PATCH = UserType 8

pattern USER_TYPE_OUTPUT_PATCH :: UserType
pattern USER_TYPE_OUTPUT_PATCH = UserType 9

pattern USER_TYPE_RASTERIZER_ORDERED_BUFFER :: UserType
pattern USER_TYPE_RASTERIZER_ORDERED_BUFFER = UserType 10

pattern USER_TYPE_RASTERIZER_ORDERED_BYTE_ADDRESS_BUFFER :: UserType
pattern USER_TYPE_RASTERIZER_ORDERED_BYTE_ADDRESS_BUFFER = UserType 11

pattern USER_TYPE_RASTERIZER_ORDERED_STRUCTURED_BUFFER :: UserType
pattern USER_TYPE_RASTERIZER_ORDERED_STRUCTURED_BUFFER = UserType 12

pattern USER_TYPE_RASTERIZER_ORDERED_TEXTURE_1D :: UserType
pattern USER_TYPE_RASTERIZER_ORDERED_TEXTURE_1D = UserType 13

pattern USER_TYPE_RASTERIZER_ORDERED_TEXTURE_1D_ARRAY :: UserType
pattern USER_TYPE_RASTERIZER_ORDERED_TEXTURE_1D_ARRAY = UserType 14

pattern USER_TYPE_RASTERIZER_ORDERED_TEXTURE_2D :: UserType
pattern USER_TYPE_RASTERIZER_ORDERED_TEXTURE_2D = UserType 15

pattern USER_TYPE_RASTERIZER_ORDERED_TEXTURE_2D_ARRAY :: UserType
pattern USER_TYPE_RASTERIZER_ORDERED_TEXTURE_2D_ARRAY = UserType 16

pattern USER_TYPE_RASTERIZER_ORDERED_TEXTURE_3D :: UserType
pattern USER_TYPE_RASTERIZER_ORDERED_TEXTURE_3D = UserType 17

pattern USER_TYPE_RAYTRACING_ACCELERATION_STRUCTURE :: UserType
pattern USER_TYPE_RAYTRACING_ACCELERATION_STRUCTURE = UserType 18

pattern USER_TYPE_RW_BUFFER :: UserType
pattern USER_TYPE_RW_BUFFER = UserType 19

pattern USER_TYPE_RW_BYTE_ADDRESS_BUFFER :: UserType
pattern USER_TYPE_RW_BYTE_ADDRESS_BUFFER = UserType 20

pattern USER_TYPE_RW_STRUCTURED_BUFFER :: UserType
pattern USER_TYPE_RW_STRUCTURED_BUFFER = UserType 21

pattern USER_TYPE_RW_TEXTURE_1D :: UserType
pattern USER_TYPE_RW_TEXTURE_1D = UserType 22

pattern USER_TYPE_RW_TEXTURE_1D_ARRAY :: UserType
pattern USER_TYPE_RW_TEXTURE_1D_ARRAY = UserType 23

pattern USER_TYPE_RW_TEXTURE_2D :: UserType
pattern USER_TYPE_RW_TEXTURE_2D = UserType 24

pattern USER_TYPE_RW_TEXTURE_2D_ARRAY :: UserType
pattern USER_TYPE_RW_TEXTURE_2D_ARRAY = UserType 25

pattern USER_TYPE_RW_TEXTURE_3D :: UserType
pattern USER_TYPE_RW_TEXTURE_3D = UserType 26

pattern USER_TYPE_STRUCTURED_BUFFER :: UserType
pattern USER_TYPE_STRUCTURED_BUFFER = UserType 27

pattern USER_TYPE_SUBPASS_INPUT :: UserType
pattern USER_TYPE_SUBPASS_INPUT = UserType 28

pattern USER_TYPE_SUBPASS_INPUT_MS :: UserType
pattern USER_TYPE_SUBPASS_INPUT_MS = UserType 29

pattern USER_TYPE_TEXTURE_1D :: UserType
pattern USER_TYPE_TEXTURE_1D = UserType 30

pattern USER_TYPE_TEXTURE_1D_ARRAY :: UserType
pattern USER_TYPE_TEXTURE_1D_ARRAY = UserType 31

pattern USER_TYPE_TEXTURE_2D :: UserType
pattern USER_TYPE_TEXTURE_2D = UserType 32

pattern USER_TYPE_TEXTURE_2D_ARRAY :: UserType
pattern USER_TYPE_TEXTURE_2D_ARRAY = UserType 33

pattern USER_TYPE_TEXTURE_2DMS :: UserType
pattern USER_TYPE_TEXTURE_2DMS = UserType 34

pattern USER_TYPE_TEXTURE_2DMS_ARRAY :: UserType
pattern USER_TYPE_TEXTURE_2DMS_ARRAY = UserType 35

pattern USER_TYPE_TEXTURE_3D :: UserType
pattern USER_TYPE_TEXTURE_3D = UserType 36

pattern USER_TYPE_TEXTURE_BUFFER :: UserType
pattern USER_TYPE_TEXTURE_BUFFER = UserType 37

pattern USER_TYPE_TEXTURE_CUBE :: UserType
pattern USER_TYPE_TEXTURE_CUBE = UserType 38

pattern USER_TYPE_TEXTURE_CUBE_ARRAY :: UserType
pattern USER_TYPE_TEXTURE_CUBE_ARRAY = UserType 39

userTypeName :: IsString label => UserType -> Maybe label
userTypeName = toLabel userTypeNames

userTypeNames :: IsString label => [(UserType, label)]
userTypeNames =
  [ (USER_TYPE_INVALID, "Invalid")
  , (USER_TYPE_CBUFFER, "cbuffer")
  , (USER_TYPE_TBUFFER, "tbuffer")
  , (USER_TYPE_APPEND_STRUCTURED_BUFFER, "AppendStructuredBuffer")
  , (USER_TYPE_BUFFER, "Buffer")
  , (USER_TYPE_BYTE_ADDRESS_BUFFER, "ByteAddressBuffer")
  , (USER_TYPE_CONSTANT_BUFFER, "ConstantBuffer")
  , (USER_TYPE_CONSUME_STRUCTURED_BUFFER, "ConsumeStructuredBuffer")
  , (USER_TYPE_INPUT_PATCH, "InputPatch")
  , (USER_TYPE_OUTPUT_PATCH, "OutputPatch")
  , (USER_TYPE_RASTERIZER_ORDERED_BUFFER, "RasterizerOrderedBuffer")
  , (USER_TYPE_RASTERIZER_ORDERED_BYTE_ADDRESS_BUFFER, "RasterizerOrderedByteAddressBuffer")
  , (USER_TYPE_RASTERIZER_ORDERED_STRUCTURED_BUFFER, "RasterizerOrderedStructuredBuffer")
  , (USER_TYPE_RASTERIZER_ORDERED_TEXTURE_1D, "RasterizerOrderedTexture1D")
  , (USER_TYPE_RASTERIZER_ORDERED_TEXTURE_1D_ARRAY, "RasterizerOrderedTexture1DArray")
  , (USER_TYPE_RASTERIZER_ORDERED_TEXTURE_2D, "RasterizerOrderedTexture2D")
  , (USER_TYPE_RASTERIZER_ORDERED_TEXTURE_2D_ARRAY, "RasterizerOrdered_Texture2DArray")
  , (USER_TYPE_RASTERIZER_ORDERED_TEXTURE_3D, "RasterizerOrderedTexture3D")
  , (USER_TYPE_RAYTRACING_ACCELERATION_STRUCTURE, "RaytracingAccelerationStructure")
  , (USER_TYPE_RW_BUFFER, "RWBuffer")
  , (USER_TYPE_RW_BYTE_ADDRESS_BUFFER, "RWByteAddressBuffer")
  , (USER_TYPE_RW_STRUCTURED_BUFFER, "RWStructuredBuffer")
  , (USER_TYPE_RW_TEXTURE_1D, "RWTexture1D")
  , (USER_TYPE_RW_TEXTURE_1D_ARRAY, "RWTexture1DArray")
  , (USER_TYPE_RW_TEXTURE_2D, "RWTexture2D")
  , (USER_TYPE_RW_TEXTURE_2D_ARRAY, "RWTexture2DArray")
  , (USER_TYPE_RW_TEXTURE_3D, "RWTexture3D")
  , (USER_TYPE_STRUCTURED_BUFFER, "StructuredBuffer")
  , (USER_TYPE_SUBPASS_INPUT, "SubpassInput")
  , (USER_TYPE_SUBPASS_INPUT_MS, "SubpassInputMS")
  , (USER_TYPE_TEXTURE_1D, "Texture1D")
  , (USER_TYPE_TEXTURE_1D_ARRAY, "Texture1DArray")
  , (USER_TYPE_TEXTURE_2D, "Texture2D")
  , (USER_TYPE_TEXTURE_2D_ARRAY, "Texture2DArray")
  , (USER_TYPE_TEXTURE_2DMS, "Texture2DMS")
  , (USER_TYPE_TEXTURE_2DMS_ARRAY, "Texture2DMSArray")
  , (USER_TYPE_TEXTURE_3D, "Texture3D")
  , (USER_TYPE_TEXTURE_BUFFER, "TextureBuffer")
  , (USER_TYPE_TEXTURE_CUBE, "TextureCube")
  , (USER_TYPE_TEXTURE_CUBE_ARRAY, "TextureCubeArray")
  ]

userTypeId :: (Eq label, IsString label) => label -> Maybe UserType
userTypeId = \case
  "Invalid" -> Just USER_TYPE_INVALID
  "cbuffer" -> Just USER_TYPE_CBUFFER
  "tbuffer" -> Just USER_TYPE_TBUFFER
  "AppendStructuredBuffer" -> Just USER_TYPE_APPEND_STRUCTURED_BUFFER
  "Buffer" -> Just USER_TYPE_BUFFER
  "ByteAddressBuffer" -> Just USER_TYPE_BYTE_ADDRESS_BUFFER
  "ConstantBuffer" -> Just USER_TYPE_CONSTANT_BUFFER
  "ConsumeStructuredBuffer" -> Just USER_TYPE_CONSUME_STRUCTURED_BUFFER
  "InputPatch" -> Just USER_TYPE_INPUT_PATCH
  "OutputPatch" -> Just USER_TYPE_OUTPUT_PATCH
  "RasterizerOrderedBuffer" -> Just USER_TYPE_RASTERIZER_ORDERED_BUFFER
  "RasterizerOrderedByteAddressBuffer" -> Just USER_TYPE_RASTERIZER_ORDERED_BYTE_ADDRESS_BUFFER
  "RasterizerOrderedStructuredBuffer" -> Just USER_TYPE_RASTERIZER_ORDERED_STRUCTURED_BUFFER
  "RasterizerOrderedTexture1D" -> Just USER_TYPE_RASTERIZER_ORDERED_TEXTURE_1D
  "RasterizerOrderedTexture1DArray" -> Just USER_TYPE_RASTERIZER_ORDERED_TEXTURE_1D_ARRAY
  "RasterizerOrderedTexture2D" -> Just USER_TYPE_RASTERIZER_ORDERED_TEXTURE_2D
  "RasterizerOrderedTexture2DArray" -> Just USER_TYPE_RASTERIZER_ORDERED_TEXTURE_2D_ARRAY
  "RasterizerOrderedTexture3D" -> Just USER_TYPE_RASTERIZER_ORDERED_TEXTURE_3D
  "RaytracingAccelerationStructure" -> Just USER_TYPE_RAYTRACING_ACCELERATION_STRUCTURE
  "RWBuffer" -> Just USER_TYPE_RW_BUFFER
  "RWByteAddressBuffer" -> Just USER_TYPE_RW_BYTE_ADDRESS_BUFFER
  "RWStructuredBuffer" -> Just USER_TYPE_RW_STRUCTURED_BUFFER
  "RWTexture1D" -> Just USER_TYPE_RW_TEXTURE_1D
  "RWTexture1DArray" -> Just USER_TYPE_RW_TEXTURE_1D_ARRAY
  "RWTexture2D" -> Just USER_TYPE_RW_TEXTURE_2D
  "RWTexture2DArray" -> Just USER_TYPE_RW_TEXTURE_2D_ARRAY
  "RWTexture3D" -> Just USER_TYPE_RW_TEXTURE_3D
  "StructuredBuffer" -> Just USER_TYPE_STRUCTURED_BUFFER
  "SubpassInput" -> Just USER_TYPE_SUBPASS_INPUT
  "SubpassInputMs" -> Just USER_TYPE_SUBPASS_INPUT_MS
  "Texture1D" -> Just USER_TYPE_TEXTURE_1D
  "Texture1DArray" -> Just USER_TYPE_TEXTURE_1D_ARRAY
  "Texture2D" -> Just USER_TYPE_TEXTURE_2D
  "Texture2DArray" -> Just USER_TYPE_TEXTURE_2D_ARRAY
  "Texture2DMS" -> Just USER_TYPE_TEXTURE_2DMS
  "Texture2DMSArray" -> Just USER_TYPE_TEXTURE_2DMS_ARRAY
  "Texture3D" -> Just USER_TYPE_TEXTURE_3D
  "TextureBuffer" -> Just USER_TYPE_TEXTURE_BUFFER
  "TextureCube" -> Just USER_TYPE_TEXTURE_CUBE
  "TextureCubeArray" -> Just USER_TYPE_TEXTURE_CUBE_ARRAY
  _ -> Nothing
