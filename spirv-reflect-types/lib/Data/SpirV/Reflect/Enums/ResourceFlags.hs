module Data.SpirV.Reflect.Enums.ResourceFlags where

import Data.SpirV.Reflect.Enums.Common

type ResourceFlags = ResourceFlagBits

newtype ResourceFlagBits = ResourceFlagBits Flags
  deriving newtype (Eq, Ord, Show, Bits, FiniteBits)

pattern RESOURCE_FLAG_UNDEFINED :: ResourceFlagBits
pattern RESOURCE_FLAG_UNDEFINED = ResourceFlagBits 0x00000000

pattern RESOURCE_FLAG_SAMPLER :: ResourceFlagBits
pattern RESOURCE_FLAG_SAMPLER = ResourceFlagBits 0x00000001

pattern RESOURCE_FLAG_CBV :: ResourceFlagBits
pattern RESOURCE_FLAG_CBV = ResourceFlagBits 0x00000002

pattern RESOURCE_FLAG_SRV :: ResourceFlagBits
pattern RESOURCE_FLAG_SRV = ResourceFlagBits 0x00000004

pattern RESOURCE_FLAG_UAV :: ResourceFlagBits
pattern RESOURCE_FLAG_UAV = ResourceFlagBits 0x00000008
