module Data.SpirV.Reflect.Enums.Generator where

import Data.SpirV.Reflect.Enums.Common

newtype Generator = Generator Int
  deriving newtype (Eq, Ord, Show, Enum)

pattern GENERATOR_KHRONOS_LLVM_SPIRV_TRANSLATOR :: Generator
pattern GENERATOR_KHRONOS_LLVM_SPIRV_TRANSLATOR = Generator 6

pattern GENERATOR_KHRONOS_SPIRV_TOOLS_ASSEMBLER :: Generator
pattern GENERATOR_KHRONOS_SPIRV_TOOLS_ASSEMBLER = Generator 7

pattern GENERATOR_KHRONOS_GLSLANG_REFERENCE_FRONT_END :: Generator
pattern GENERATOR_KHRONOS_GLSLANG_REFERENCE_FRONT_END = Generator 8

pattern GENERATOR_GOOGLE_SHADERC_OVER_GLSLANG :: Generator
pattern GENERATOR_GOOGLE_SHADERC_OVER_GLSLANG = Generator 13

pattern GENERATOR_GOOGLE_SPIREGG :: Generator
pattern GENERATOR_GOOGLE_SPIREGG = Generator 14

pattern GENERATOR_GOOGLE_RSPIRV :: Generator
pattern GENERATOR_GOOGLE_RSPIRV = Generator 15

pattern GENERATOR_X_LEGEND_MESA_MESAIR_SPIRV_TRANSLATOR :: Generator
pattern GENERATOR_X_LEGEND_MESA_MESAIR_SPIRV_TRANSLATOR = Generator 16

pattern GENERATOR_KHRONOS_SPIRV_TOOLS_LINKER :: Generator
pattern GENERATOR_KHRONOS_SPIRV_TOOLS_LINKER = Generator 17

pattern GENERATOR_WINE_VKD3D_SHADER_COMPILER :: Generator
pattern GENERATOR_WINE_VKD3D_SHADER_COMPILER = Generator 18

pattern GENERATOR_CLAY_CLAY_SHADER_COMPILER :: Generator
pattern GENERATOR_CLAY_CLAY_SHADER_COMPILER = Generator 19

generatorName :: IsString label => Generator -> Maybe label
generatorName = toLabel generatorNames

generatorNames :: IsString label => [(Generator, label)]
generatorNames =
  [ (GENERATOR_KHRONOS_LLVM_SPIRV_TRANSLATOR, "Khronos LLVM/SPIR-V Translator")
  , (GENERATOR_KHRONOS_SPIRV_TOOLS_ASSEMBLER, "Khronos SPIR-V Tools Assembler")
  , (GENERATOR_KHRONOS_GLSLANG_REFERENCE_FRONT_END, "Khronos Glslang Reference Front End")
  , (GENERATOR_GOOGLE_SHADERC_OVER_GLSLANG, "Google Shaderc over Glslang")
  , (GENERATOR_GOOGLE_SPIREGG, "Google spiregg")
  , (GENERATOR_GOOGLE_RSPIRV, "Google rspirv")
  , (GENERATOR_X_LEGEND_MESA_MESAIR_SPIRV_TRANSLATOR, "X-LEGEND Mesa-IR/SPIR-V Translator")
  , (GENERATOR_KHRONOS_SPIRV_TOOLS_LINKER, "Khronos SPIR-V Tools Linker")
  , (GENERATOR_WINE_VKD3D_SHADER_COMPILER, "Wine VKD3D Shader Compiler")
  , (GENERATOR_CLAY_CLAY_SHADER_COMPILER, "Clay Clay Shader Compiler")
  ]
