module Data.SpirV.Reflect.Enums.DecorationFlags where

import Data.SpirV.Reflect.Enums.Common

type DecorationFlags = DecorationFlagBits

newtype DecorationFlagBits = DecorationFlagBits Flags
  deriving newtype (Eq, Ord, Show, Bits, FiniteBits)

pattern DECORATION_NONE :: DecorationFlagBits
pattern DECORATION_NONE = DecorationFlagBits 0x00000000

pattern DECORATION_BLOCK :: DecorationFlagBits
pattern DECORATION_BLOCK = DecorationFlagBits 0x00000001

pattern DECORATION_BUFFER_BLOCK :: DecorationFlagBits
pattern DECORATION_BUFFER_BLOCK = DecorationFlagBits 0x00000002

pattern DECORATION_ROW_MAJOR :: DecorationFlagBits
pattern DECORATION_ROW_MAJOR = DecorationFlagBits 0x00000004

pattern DECORATION_COLUMN_MAJOR :: DecorationFlagBits
pattern DECORATION_COLUMN_MAJOR = DecorationFlagBits 0x00000008

pattern DECORATION_BUILT_IN :: DecorationFlagBits
pattern DECORATION_BUILT_IN = DecorationFlagBits 0x00000010

pattern DECORATION_NOPERSPECTIVE :: DecorationFlagBits
pattern DECORATION_NOPERSPECTIVE = DecorationFlagBits 0x00000020

pattern DECORATION_FLAT :: DecorationFlagBits
pattern DECORATION_FLAT = DecorationFlagBits 0x00000040

pattern DECORATION_NON_WRITABLE :: DecorationFlagBits
pattern DECORATION_NON_WRITABLE = DecorationFlagBits 0x00000080

pattern DECORATION_RELAXED_PRECISION :: DecorationFlagBits
pattern DECORATION_RELAXED_PRECISION = DecorationFlagBits 0x00000100

pattern DECORATION_NON_READABLE :: DecorationFlagBits
pattern DECORATION_NON_READABLE = DecorationFlagBits 0x00000200

pattern DECORATION_PATCH :: DecorationFlagBits
pattern DECORATION_PATCH = DecorationFlagBits 0x00000400

pattern DECORATION_PER_VERTEX :: DecorationFlagBits
pattern DECORATION_PER_VERTEX = DecorationFlagBits 0x00000800

pattern DECORATION_PER_TASK :: DecorationFlagBits
pattern DECORATION_PER_TASK = DecorationFlagBits 0x00001000

pattern DECORATION_WEIGHT_TEXTURE :: DecorationFlagBits
pattern DECORATION_WEIGHT_TEXTURE = DecorationFlagBits 0x00002000

pattern DECORATION_BLOCK_MATCH_TEXTURE :: DecorationFlagBits
pattern DECORATION_BLOCK_MATCH_TEXTURE = DecorationFlagBits 0x00004000
