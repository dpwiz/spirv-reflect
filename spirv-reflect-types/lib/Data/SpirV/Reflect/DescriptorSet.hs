module Data.SpirV.Reflect.DescriptorSet where

import Data.Vector (Vector)
import Data.Word (Word32)
import GHC.Generics (Generic)

import Data.SpirV.Reflect.DescriptorBinding (DescriptorBinding)

data DescriptorSet = DescriptorSet
  { set      :: Word32
  , bindings :: Vector DescriptorBinding
  }
  deriving (Eq, Show, Generic)
