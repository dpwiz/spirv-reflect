module Data.SpirV.Reflect.Module
  ( Module(..)
  ) where

import Data.Text (Text)
import Data.Vector (Vector)
import GHC.Generics (Generic)

import Data.SpirV.Reflect.BlockVariable (BlockVariable)
import Data.SpirV.Reflect.DescriptorBinding (DescriptorBinding)
import Data.SpirV.Reflect.DescriptorSet (DescriptorSet)
import Data.SpirV.Reflect.Enums qualified as Enums
import Data.SpirV.Reflect.InterfaceVariable (InterfaceVariable)
import Data.SpirV.Reflect.SpecializationConstant (SpecializationConstant)

data Module = Module
  { generator               :: Enums.Generator
  , entry_point_name        :: Text
  , entry_point_id          :: Int
  , source_language         :: Int
  , source_language_version :: Int
  , spirv_execution_model   :: Int
  , shader_stage            :: Int
  , descriptor_bindings     :: Vector DescriptorBinding
  , descriptor_sets         :: Vector DescriptorSet
  , input_variables         :: Vector InterfaceVariable
  , output_variables        :: Vector InterfaceVariable
  , push_constants          :: Vector BlockVariable
  , spec_constants          :: Vector SpecializationConstant
  }
  deriving (Eq, Show, Generic)
