module Data.SpirV.Reflect.TypeDescription where

import Data.SpirV.Enum qualified as SpirV
import Data.SpirV.Reflect.Enums qualified as Reflect
import Data.SpirV.Reflect.Traits qualified as Traits
import Data.Text (Text)
import Data.Vector (Vector)
import Data.Word (Word32)
import GHC.Generics (Generic)

data TypeDescription = TypeDescription
  { id                      :: Maybe Word32
  , op                      :: Maybe SpirV.Op
  , type_name               :: Maybe Text
  , struct_member_name      :: Maybe Text
  , storage_class           :: SpirV.StorageClass
  , type_flags              :: Reflect.TypeFlags
  , traits                  :: Maybe Traits
  , struct_type_description :: Maybe TypeDescription -- ^ If underlying type is a struct (ex. array of structs) this gives access to the 'OpTypeStruct'.
  , copied                  :: Maybe Word32 -- ^ Some pointers to SpvReflectTypeDescription are really just copies of another reference to the same OpType.
  , members                 :: Vector TypeDescription -- XXX: deprecated
  }
  deriving (Eq, Show, Generic)

data Traits = Traits
  { numeric :: Traits.Numeric
  , image   :: Traits.Image
  , array   :: Traits.Array
  }
  deriving (Eq, Show, Generic)

emptyTraits :: Traits
emptyTraits = Traits
  { numeric = Traits.emptyNumeric
  , image   = Traits.emptyImage
  , array   = Traits.emptyArray
  }
