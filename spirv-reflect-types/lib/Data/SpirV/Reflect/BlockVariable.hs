module Data.SpirV.Reflect.BlockVariable
  ( BlockVariable(..)
  , WordOffset(..)
  ) where

import Data.Text (Text)
import Data.Vector (Vector)
import GHC.Generics (Generic)
import Data.Word (Word32)

import Data.SpirV.Reflect.Enums qualified as Enums
import Data.SpirV.Reflect.Traits qualified as Traits
import Data.SpirV.Reflect.TypeDescription (TypeDescription)

data BlockVariable = BlockVariable
  { spirv_id         :: Maybe Word32
  , name             :: Maybe Text
  , offset           :: Word32
  , absolute_offset  :: Word32
  , size             :: Word32
  , padded_size      :: Word32
  , decorations      :: Enums.DecorationFlags
  , numeric          :: Traits.Numeric
  , array            :: Traits.Array
  , members          :: Vector BlockVariable
  , type_description :: Maybe TypeDescription
  , word_offset      :: Maybe WordOffset
  }
  deriving (Eq, Show, Generic)

newtype WordOffset = WordOffset
  { offset :: Word32
  }
  deriving (Eq, Show, Generic)
