# Changelog for spirv-reflect-types

## [0.3] - 2024-07-07

* Update types to accomodate `vulkan-sdk-1.3.283.0`.

[0.3]: https://gitlab.com/dpwiz/spirv-reflect/-/tree/v0.3-types

## [0.2] - 2022-09-04

* Update types to accomodate FFI loader.

[0.2]: https://gitlab.com/dpwiz/spirv-reflect/-/tree/v0.2-types

## [0.1] - 2022-06-11

* Initial release.

[0.1]: https://gitlab.com/dpwiz/spirv-reflect/-/tree/v0.1-types
