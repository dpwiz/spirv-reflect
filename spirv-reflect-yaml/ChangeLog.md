# Changelog for spirv-reflect-yaml

## [0.3] - 2024-07-07

* Update types and parser to `vulkan-sdk-1.3.283.0`.

[0.3]: https://gitlab.com/dpwiz/spirv-reflect/-/tree/v0.3-yaml

## [0.2] - 2022-09-04

* Update loader to match FFI types.

[0.2]: https://gitlab.com/dpwiz/spirv-reflect/-/tree/v0.2-yaml


## [0.1] - 2022-06-11

* Initial release.

[0.1]: https://gitlab.com/dpwiz/spirv-reflect/-/tree/v0.1-yaml
