# Changelog for spirv-reflect-ffi

## [0.3] - 2024-09-30

* Update cbits to `vulkan-sdk-1.3.290`.
* Updates for `spirv-reflect-types-0.3`.

[0.3]: https://gitlab.com/dpwiz/spirv-reflect/-/tree/v0.3-ffi

## [0.2] - 2022-09-04

* Fixed a mess in input/output interface variables.

[0.2]: https://gitlab.com/dpwiz/spirv-reflect/-/tree/v0.2-ffi

## [0.1] - 2022-09-04

* Initial release.

[0.1]: https://gitlab.com/dpwiz/spirv-reflect/-/tree/v0.1-ffi
